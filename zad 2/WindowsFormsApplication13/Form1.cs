﻿//2.b)
//Napravite jednostavnu igru vješala. POjmovi se učitaju u listu iz datoteke,i usvakoj partiji se odabire nasumični 
//pojam iz liste. Omogućiti svu funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala, dovoljno
//je na labeli ispisati koliko je pokušaja za odabir slova preostalo.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication13
{
    public partial class Vjesalo : Form
    {
        Random rand = new Random();
        List<string> list = new List<string>();
        int brojPokusaja;
        string IzListe, uLabelu;
        public void Reset()
        {
            IzListe = list[rand.Next(0, list.Count - 1)];
            uLabelu = new string('*', IzListe.Length);
            lbl_Rijec.Text = uLabelu;
            brojPokusaja = 10;
            lbl_Rezultat.Text = brojPokusaja.ToString();
        }
        public Vjesalo()
        {
            InitializeComponent();
        }

        private void btn_Izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Vjesalo_Load(object sender, EventArgs e)
        {
            string line;
            using (System.IO.StreamReader reader = new System.IO.StreamReader (@"C:\Vješalo\vjesalo.txt"))
            
            {
                while((line=reader.ReadLine())!=null)
                {
                    list.Add(line);
                }
                Reset();
            }
        }

        private void lbl_Rezultat_Click(object sender, EventArgs e)
        {

        }

        private void btn_Unesi_Click(object sender, EventArgs e)
        {
            if(txtBox_UnesiRijec.Text.Length==1)
            {
                if(IzListe.Contains(txtBox_UnesiRijec.Text))
                {
                    string trenutna_rijec = IzListe;
                    while (trenutna_rijec.Contains(txtBox_UnesiRijec.Text))
                    {
                        int i = trenutna_rijec.IndexOf(txtBox_UnesiRijec.Text);
                        StringBuilder builder1 = new StringBuilder(trenutna_rijec);
                        builder1[i] = '*';
                        trenutna_rijec = builder1.ToString();
                        StringBuilder builder2 = new StringBuilder(uLabelu);
                        builder2[i] = Convert.ToChar(txtBox_UnesiRijec.Text);
                        uLabelu = builder2.ToString();
                    }
                    lbl_Rijec.Text = uLabelu;
                    if(uLabelu==IzListe)
                    {
                        MessageBox.Show("Pobjeda!");
                        Reset();
                    }
                }
                else
                {
                    brojPokusaja--;
                    lbl_Rezultat.Text = brojPokusaja.ToString();
                    if(brojPokusaja<=0)
                    {
                        MessageBox.Show("Izgubili ste.");
                        Reset();
                    }
                }
            }
            else if(txtBox_UnesiRijec.Text.Length>1)
            {
                if(IzListe==txtBox_UnesiRijec.Text)
                {
                    MessageBox.Show("Pobjeda!");
                    Reset();
                }
                else
                {
                    brojPokusaja--;
                    lbl_Rezultat.Text = brojPokusaja.ToString();
                    if(brojPokusaja<=0)
                    {
                        MessageBox.Show("Izgubili ste.");
                        Reset();
                    }
                }
            }
        }
    }
}
