﻿namespace WindowsFormsApplication13
{
    partial class Vjesalo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Izlaz = new System.Windows.Forms.Button();
            this.btn_Unesi = new System.Windows.Forms.Button();
            this.txtBox_UnesiRijec = new System.Windows.Forms.TextBox();
            this.lbl_Rezultat = new System.Windows.Forms.Label();
            this.lbl_Rijec = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Izlaz
            // 
            this.btn_Izlaz.Location = new System.Drawing.Point(252, 202);
            this.btn_Izlaz.Name = "btn_Izlaz";
            this.btn_Izlaz.Size = new System.Drawing.Size(75, 23);
            this.btn_Izlaz.TabIndex = 0;
            this.btn_Izlaz.Text = "Izlaz";
            this.btn_Izlaz.UseVisualStyleBackColor = true;
            this.btn_Izlaz.Click += new System.EventHandler(this.btn_Izlaz_Click);
            // 
            // btn_Unesi
            // 
            this.btn_Unesi.Location = new System.Drawing.Point(252, 71);
            this.btn_Unesi.Name = "btn_Unesi";
            this.btn_Unesi.Size = new System.Drawing.Size(75, 23);
            this.btn_Unesi.TabIndex = 1;
            this.btn_Unesi.Text = "Unesi";
            this.btn_Unesi.UseVisualStyleBackColor = true;
            this.btn_Unesi.Click += new System.EventHandler(this.btn_Unesi_Click);
            // 
            // txtBox_UnesiRijec
            // 
            this.txtBox_UnesiRijec.Location = new System.Drawing.Point(119, 71);
            this.txtBox_UnesiRijec.Name = "txtBox_UnesiRijec";
            this.txtBox_UnesiRijec.Size = new System.Drawing.Size(42, 20);
            this.txtBox_UnesiRijec.TabIndex = 2;
            // 
            // lbl_Rezultat
            // 
            this.lbl_Rezultat.AutoSize = true;
            this.lbl_Rezultat.Location = new System.Drawing.Point(116, 155);
            this.lbl_Rezultat.Name = "lbl_Rezultat";
            this.lbl_Rezultat.Size = new System.Drawing.Size(0, 13);
            this.lbl_Rezultat.TabIndex = 3;
            this.lbl_Rezultat.Click += new System.EventHandler(this.lbl_Rezultat_Click);
            // 
            // lbl_Rijec
            // 
            this.lbl_Rijec.AutoSize = true;
            this.lbl_Rijec.Location = new System.Drawing.Point(42, 78);
            this.lbl_Rijec.Name = "lbl_Rijec";
            this.lbl_Rijec.Size = new System.Drawing.Size(31, 13);
            this.lbl_Rijec.TabIndex = 4;
            this.lbl_Rijec.Text = "Riječ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Broj pokušaja";
            // 
            // Vjesalo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 265);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_Rijec);
            this.Controls.Add(this.lbl_Rezultat);
            this.Controls.Add(this.txtBox_UnesiRijec);
            this.Controls.Add(this.btn_Unesi);
            this.Controls.Add(this.btn_Izlaz);
            this.Name = "Vjesalo";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Vjesalo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Izlaz;
        private System.Windows.Forms.Button btn_Unesi;
        private System.Windows.Forms.TextBox txtBox_UnesiRijec;
        private System.Windows.Forms.Label lbl_Rezultat;
        private System.Windows.Forms.Label lbl_Rijec;
        private System.Windows.Forms.Label label1;
    }
}

