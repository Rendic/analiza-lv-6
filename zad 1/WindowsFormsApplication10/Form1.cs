﻿//2.a)
//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost znanstvenog kalkulatora, odnosno implementirati
//osnovne (+,-,*,/) i barem 5 naprednih(sin,cos,log,sqrt...) operacija.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication10
{
    public partial class Kalkulator : Form
    {
        double op1, op2, rez;
        public Kalkulator()
        {
            InitializeComponent();
        }

        private void btn_Zbrajanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1)) 
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            if (!double.TryParse(txtBox_op2.Text, out op2))
            {
                MessageBox.Show("Krivi unos drugog operanda!");
            }
            rez = op1 + op2;
        }

        private void btn_Oduzimanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            if (!double.TryParse(txtBox_op2.Text, out op2))
            {
                MessageBox.Show("Krivi unos drugog operanda!");
            }
            rez = op1 - op2;
        }

        private void btn_Mnozenje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            if (!double.TryParse(txtBox_op2.Text, out op2))
            {
                MessageBox.Show("Krivi unos drugog operanda!");
            }
            rez = op1 * op2;
        }

        private void btn_Dijeljenje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1))
            {
                MessageBox.Show("Krivi unos prvog operanda!");
            }
            if (!double.TryParse(txtBox_op2.Text, out op2) || op1==0)
            {
                MessageBox.Show("Krivi unos drugog operanda!");
            }
            rez = op1 / op2;
        }

        private void btn_Sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1))
            {
                MessageBox.Show("Krivi unos operanda!");
            }
             rez = Math.Sin(op1);
        }

        private void btn_Cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1))
            {
                MessageBox.Show("Krivi unos operanda!");
            }
            rez = Math.Cos(op1);
        }

        private void btn_Logaritam_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1) || op1 <= 0)
            {
                MessageBox.Show("Krivi unos operanda!");
            }
            rez = Math.Log(op1);
        }

        private void btn_DrugiKorijen_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1) || op1<0)
            {
                MessageBox.Show("Krivi unos operanda!");
            }
            rez = Math.Sqrt(op1);
        }

        private void btn_Kub_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox_op1.Text, out op1))
            {
                MessageBox.Show("Krivi unos operanda!");
            }
            rez = op1 * op1 * op1;
        }

        private void btn_Izracunaj_Click(object sender, EventArgs e)
        {
            txtBox_rezultat.Text = rez.ToString();
            txtBox_rezultat.Show();
            txtBox_op1.Clear();
            txtBox_op2.Clear();
        }
        private void btn_Izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
