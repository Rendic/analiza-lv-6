﻿namespace WindowsFormsApplication10
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_op1 = new System.Windows.Forms.Label();
            this.lbl_op2 = new System.Windows.Forms.Label();
            this.txtBox_op1 = new System.Windows.Forms.TextBox();
            this.txtBox_op2 = new System.Windows.Forms.TextBox();
            this.lbl_rezultat = new System.Windows.Forms.Label();
            this.btn_Zbrajanje = new System.Windows.Forms.Button();
            this.btn_Mnozenje = new System.Windows.Forms.Button();
            this.btn_Dijeljenje = new System.Windows.Forms.Button();
            this.btn_Sin = new System.Windows.Forms.Button();
            this.btn_Cos = new System.Windows.Forms.Button();
            this.btn_DrugiKorijen = new System.Windows.Forms.Button();
            this.btn_Kub = new System.Windows.Forms.Button();
            this.btn_Izracunaj = new System.Windows.Forms.Button();
            this.btn_Oduzimanje = new System.Windows.Forms.Button();
            this.btn_Logaritam = new System.Windows.Forms.Button();
            this.txtBox_rezultat = new System.Windows.Forms.TextBox();
            this.btn_Izlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_op1
            // 
            this.lbl_op1.AutoSize = true;
            this.lbl_op1.Location = new System.Drawing.Point(40, 43);
            this.lbl_op1.Name = "lbl_op1";
            this.lbl_op1.Size = new System.Drawing.Size(60, 13);
            this.lbl_op1.TabIndex = 0;
            this.lbl_op1.Text = "Operand 1:";
            // 
            // lbl_op2
            // 
            this.lbl_op2.AutoSize = true;
            this.lbl_op2.Location = new System.Drawing.Point(40, 101);
            this.lbl_op2.Name = "lbl_op2";
            this.lbl_op2.Size = new System.Drawing.Size(60, 13);
            this.lbl_op2.TabIndex = 1;
            this.lbl_op2.Text = "Operand 2:";
            // 
            // txtBox_op1
            // 
            this.txtBox_op1.Location = new System.Drawing.Point(117, 40);
            this.txtBox_op1.Name = "txtBox_op1";
            this.txtBox_op1.Size = new System.Drawing.Size(100, 20);
            this.txtBox_op1.TabIndex = 3;
            // 
            // txtBox_op2
            // 
            this.txtBox_op2.Location = new System.Drawing.Point(117, 98);
            this.txtBox_op2.Name = "txtBox_op2";
            this.txtBox_op2.Size = new System.Drawing.Size(100, 20);
            this.txtBox_op2.TabIndex = 4;
            // 
            // lbl_rezultat
            // 
            this.lbl_rezultat.AutoSize = true;
            this.lbl_rezultat.Location = new System.Drawing.Point(239, 420);
            this.lbl_rezultat.Name = "lbl_rezultat";
            this.lbl_rezultat.Size = new System.Drawing.Size(49, 13);
            this.lbl_rezultat.TabIndex = 5;
            this.lbl_rezultat.Text = "Rezultat:";
            // 
            // btn_Zbrajanje
            // 
            this.btn_Zbrajanje.Location = new System.Drawing.Point(43, 157);
            this.btn_Zbrajanje.Name = "btn_Zbrajanje";
            this.btn_Zbrajanje.Size = new System.Drawing.Size(75, 23);
            this.btn_Zbrajanje.TabIndex = 6;
            this.btn_Zbrajanje.Text = "Zbrajanje";
            this.btn_Zbrajanje.UseVisualStyleBackColor = true;
            this.btn_Zbrajanje.Click += new System.EventHandler(this.btn_Zbrajanje_Click);
            // 
            // btn_Mnozenje
            // 
            this.btn_Mnozenje.Location = new System.Drawing.Point(43, 260);
            this.btn_Mnozenje.Name = "btn_Mnozenje";
            this.btn_Mnozenje.Size = new System.Drawing.Size(75, 23);
            this.btn_Mnozenje.TabIndex = 7;
            this.btn_Mnozenje.Text = "Množenje";
            this.btn_Mnozenje.UseVisualStyleBackColor = true;
            this.btn_Mnozenje.Click += new System.EventHandler(this.btn_Mnozenje_Click);
            // 
            // btn_Dijeljenje
            // 
            this.btn_Dijeljenje.Location = new System.Drawing.Point(43, 311);
            this.btn_Dijeljenje.Name = "btn_Dijeljenje";
            this.btn_Dijeljenje.Size = new System.Drawing.Size(75, 23);
            this.btn_Dijeljenje.TabIndex = 8;
            this.btn_Dijeljenje.Text = "Dijeljenje";
            this.btn_Dijeljenje.UseVisualStyleBackColor = true;
            this.btn_Dijeljenje.Click += new System.EventHandler(this.btn_Dijeljenje_Click);
            // 
            // btn_Sin
            // 
            this.btn_Sin.Location = new System.Drawing.Point(43, 364);
            this.btn_Sin.Name = "btn_Sin";
            this.btn_Sin.Size = new System.Drawing.Size(75, 23);
            this.btn_Sin.TabIndex = 9;
            this.btn_Sin.Text = "Sinus";
            this.btn_Sin.UseVisualStyleBackColor = true;
            this.btn_Sin.Click += new System.EventHandler(this.btn_Sin_Click);
            // 
            // btn_Cos
            // 
            this.btn_Cos.Location = new System.Drawing.Point(43, 415);
            this.btn_Cos.Name = "btn_Cos";
            this.btn_Cos.Size = new System.Drawing.Size(75, 23);
            this.btn_Cos.TabIndex = 10;
            this.btn_Cos.Text = "Kosinus";
            this.btn_Cos.UseVisualStyleBackColor = true;
            this.btn_Cos.Click += new System.EventHandler(this.btn_Cos_Click);
            // 
            // btn_DrugiKorijen
            // 
            this.btn_DrugiKorijen.Location = new System.Drawing.Point(188, 260);
            this.btn_DrugiKorijen.Name = "btn_DrugiKorijen";
            this.btn_DrugiKorijen.Size = new System.Drawing.Size(75, 23);
            this.btn_DrugiKorijen.TabIndex = 11;
            this.btn_DrugiKorijen.Text = "Drugi korijen";
            this.btn_DrugiKorijen.UseVisualStyleBackColor = true;
            this.btn_DrugiKorijen.Click += new System.EventHandler(this.btn_DrugiKorijen_Click);
            // 
            // btn_Kub
            // 
            this.btn_Kub.Location = new System.Drawing.Point(188, 311);
            this.btn_Kub.Name = "btn_Kub";
            this.btn_Kub.Size = new System.Drawing.Size(75, 23);
            this.btn_Kub.TabIndex = 12;
            this.btn_Kub.Text = "Kub";
            this.btn_Kub.UseVisualStyleBackColor = true;
            this.btn_Kub.Click += new System.EventHandler(this.btn_Kub_Click);
            // 
            // btn_Izracunaj
            // 
            this.btn_Izracunaj.Location = new System.Drawing.Point(321, 364);
            this.btn_Izracunaj.Name = "btn_Izracunaj";
            this.btn_Izracunaj.Size = new System.Drawing.Size(75, 23);
            this.btn_Izracunaj.TabIndex = 13;
            this.btn_Izracunaj.Text = "Izračunaj";
            this.btn_Izracunaj.UseVisualStyleBackColor = true;
            this.btn_Izracunaj.Click += new System.EventHandler(this.btn_Izracunaj_Click);
            // 
            // btn_Oduzimanje
            // 
            this.btn_Oduzimanje.Location = new System.Drawing.Point(43, 206);
            this.btn_Oduzimanje.Name = "btn_Oduzimanje";
            this.btn_Oduzimanje.Size = new System.Drawing.Size(75, 23);
            this.btn_Oduzimanje.TabIndex = 14;
            this.btn_Oduzimanje.Text = "Oduzimanje";
            this.btn_Oduzimanje.UseVisualStyleBackColor = true;
            this.btn_Oduzimanje.Click += new System.EventHandler(this.btn_Oduzimanje_Click);
            // 
            // btn_Logaritam
            // 
            this.btn_Logaritam.Location = new System.Drawing.Point(188, 206);
            this.btn_Logaritam.Name = "btn_Logaritam";
            this.btn_Logaritam.Size = new System.Drawing.Size(75, 23);
            this.btn_Logaritam.TabIndex = 15;
            this.btn_Logaritam.Text = "Logaritam";
            this.btn_Logaritam.UseVisualStyleBackColor = true;
            this.btn_Logaritam.Click += new System.EventHandler(this.btn_Logaritam_Click);
            // 
            // txtBox_rezultat
            // 
            this.txtBox_rezultat.Location = new System.Drawing.Point(307, 417);
            this.txtBox_rezultat.Name = "txtBox_rezultat";
            this.txtBox_rezultat.Size = new System.Drawing.Size(100, 20);
            this.txtBox_rezultat.TabIndex = 16;
            // 
            // btn_Izlaz
            // 
            this.btn_Izlaz.Location = new System.Drawing.Point(321, 464);
            this.btn_Izlaz.Name = "btn_Izlaz";
            this.btn_Izlaz.Size = new System.Drawing.Size(75, 23);
            this.btn_Izlaz.TabIndex = 17;
            this.btn_Izlaz.Text = "Izlaz";
            this.btn_Izlaz.UseVisualStyleBackColor = true;
            this.btn_Izlaz.Click += new System.EventHandler(this.btn_Izlaz_Click);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 510);
            this.Controls.Add(this.btn_Izlaz);
            this.Controls.Add(this.txtBox_rezultat);
            this.Controls.Add(this.btn_Logaritam);
            this.Controls.Add(this.btn_Oduzimanje);
            this.Controls.Add(this.btn_Izracunaj);
            this.Controls.Add(this.btn_Kub);
            this.Controls.Add(this.btn_DrugiKorijen);
            this.Controls.Add(this.btn_Cos);
            this.Controls.Add(this.btn_Sin);
            this.Controls.Add(this.btn_Dijeljenje);
            this.Controls.Add(this.btn_Mnozenje);
            this.Controls.Add(this.btn_Zbrajanje);
            this.Controls.Add(this.lbl_rezultat);
            this.Controls.Add(this.txtBox_op2);
            this.Controls.Add(this.txtBox_op1);
            this.Controls.Add(this.lbl_op2);
            this.Controls.Add(this.lbl_op1);
            this.Name = "Kalkulator";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_op1;
        private System.Windows.Forms.Label lbl_op2;
        private System.Windows.Forms.TextBox txtBox_op1;
        private System.Windows.Forms.TextBox txtBox_op2;
        private System.Windows.Forms.Label lbl_rezultat;
        private System.Windows.Forms.Button btn_Zbrajanje;
        private System.Windows.Forms.Button btn_Mnozenje;
        private System.Windows.Forms.Button btn_Dijeljenje;
        private System.Windows.Forms.Button btn_Sin;
        private System.Windows.Forms.Button btn_Cos;
        private System.Windows.Forms.Button btn_DrugiKorijen;
        private System.Windows.Forms.Button btn_Kub;
        private System.Windows.Forms.Button btn_Izracunaj;
        private System.Windows.Forms.Button btn_Oduzimanje;
        private System.Windows.Forms.Button btn_Logaritam;
        private System.Windows.Forms.TextBox txtBox_rezultat;
        private System.Windows.Forms.Button btn_Izlaz;
    }
}

